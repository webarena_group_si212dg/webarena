<?php $this->assign('title', 'Login');?>


<?php

// S'INSCRIRE

echo $this->Form->create("Players", ['context' => ['validator' => 'register'] ]);

// Email
echo $this->Form->input('email',['type'=>'email']);

// Password
echo $this->Form->input('password', ['type' => 'password']);

// Confirm password
echo $this->Form->input('confirmpassword', ['type' => 'password']);

//Bouton de confirmation
echo $this->Form->button('Créer un compte');

//Fin formulaire
echo $this->Form->end();

//echo $erreur;
?>